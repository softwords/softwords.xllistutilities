﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;

namespace XlListUtilities
{
	public class ListExtractor
	{
		public string SheetName { get; set; }
		public string WorkbookName { get; set; }

		public string DestinationFolder { get; set; }

		public bool Sql { get; set; }

		public void Extractor()
		{
			OfficeOpenXml.ExcelPackage p;
			using (System.IO.FileStream fstrm = new System.IO.FileStream(WorkbookName, System.IO.FileMode.Open))
			{
				// create an Excel workbook object from the stream
				p = new ExcelPackage(fstrm);
			}
			ExcelWorksheet wk = p.Workbook.Worksheets[SheetName];
			// get the first table off that sheet...
			OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];
			// create a ListObject from that table
			var lo = new ListObject(tbl);
			if (Sql)
			{
				string s = lo.ToXml().ToString();
				s = s.Replace("'", "''");

				using (System.IO.StreamWriter file =
					new System.IO.StreamWriter(DestinationFolder))
				{
					file.WriteLine("declare @xml xml = convert( xml, '");
					file.Write(s);
					file.WriteLine();
					file.WriteLine("')");
					file.WriteLine();
					file.WriteLine("exec dbo.procname @xml");
					file.WriteLine();
				}

			}
			else
			{
				lo.ToXml().Save(DestinationFolder);
			}
			

		}	
	}
}
